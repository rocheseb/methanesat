import os
import numpy as np
from shutil import copyfile
from typing import Optional, Sequence, Tuple, Union, Annotated, List, Generator
from netCDF4 import Dataset
import dask
import dask.array as da
from dask import delayed
from dask.diagnostics import ProgressBar
import argparse
import pandas as pd


def split_ranges(x: Sequence[bool], gap: int = 1) -> Sequence[Sequence[bool]]:
    """
    Convert input boolean array to indices and split where gaps in indices are bigger than gap
    e.g. split_ranges([1,2,3,5,6,7],gap=1) gives [[1,2,3],[5,6,7]]
    """

    indices = np.where(x)[0]
    split_indices = np.where(np.diff(indices) > gap)[0] + 1
    range_list = np.split(indices, split_indices)

    return range_list


def adaptative_resampling(
    x: Sequence[float],
    y: Sequence[float],
    resampling_factor_list: Annotated[List[float], 3],
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Resample ISRF with resampling_factor_list=[fast,med,slow] resampling factors.

    The fine resampling will be used in the ISRF center and the coarse resampling will be used in the wings. 
    """

    if not np.array_equal(resampling_factor_list, np.sort(resampling_factor_list)):
        print("WARNING: The resampling factors should be increasing")

    dx = 0.001  # nm

    # first order derivatives
    y_first = pd.Series(np.gradient(y, dx)).abs()

    # second order derivatives
    y_second = pd.Series(np.gradient(y_first, dx)).abs()

    # normalize for more convenient plotting and thresholding
    y_first = y_first / y_first.max()
    y_second = y_second / y_second.max()

    # smooth the second order derivatives for the thresholding
    y_second_roll = y_second.rolling(20, min_periods=1, center=True).mean()

    fast = y_second_roll > 0.7
    med = np.logical_and(y_second_roll > 0.4, ~fast)
    slow = np.logical_and(~fast, ~med)

    # put the different ranges in order
    range_list = np.array(
        [
            (i, speed)
            for speed, val in {"fast": fast, "med": med, "slow": slow}.items()
            for i in split_ranges(val)
        ],
        dtype=object,
    )
    order_slice = np.argsort([i[0][0] for i in range_list])
    range_list = range_list[order_slice]

    # apply the resamplings factors
    resampling_dict = {"fast": 0, "med": 1, "slow": 2}
    resample_ids = np.array([], dtype=int)
    for rng in range_list:
        resample_ids = np.append(
            resample_ids,
            [
                i % resampling_factor_list[resampling_dict[rng[1]]] == 0
                for i, val in enumerate(rng[0])
            ],
        ).astype(bool)

    return resample_ids


def resample_isrf(
    infile: str,
    outfile: str,
    resampling_factor: Optional[int] = None,
    dlambda: Optional[float] = None,
    resampling_factor_list: Optional[Annotated[List[float], 3]] = None,
) -> None:
    pass

    with Dataset(infile) as nc_in, Dataset(outfile, "w") as nc_out:
        nc_out.createDimension("x1", 1280)
        nc_out.createDimension("o", 1)
        nc_out.createDimension("w1", 15)
        nc_out.createVariable("NumberOfBands", np.int16, ("o",))
        nc_out["NumberOfBands"][:] = 1

        nc_out.createGroup("Band1")
        band1 = nc_out["Band1"]
        band1.createVariable("CenterWavelength", np.float64, ("w1",))
        band1["CenterWavelength"][:] = nc_in["Band1/CenterWavelength"][:]

        band1.createVariable("ISRFTypeIndex", np.int16, ("o",))
        band1["ISRFTypeIndex"][:] = 0

        isrf_in = nc_in["Band1/ISRF"][:]
        dw_in = nc_in["Band1/DeltaWavelength"][:]

        if resampling_factor:
            resample_ids = np.array(
                [i % resampling_factor == 0 for i, val in enumerate(dw_in)]
            )
            resample_ids[
                -1
            ] = True  # keep the last point so the full range does not change
            new_dw = dw_in[resample_ids]
            new_isrf = isrf_in[:, :, resample_ids]
        elif resampling_factor_list:
            mean_isrf = np.nanmean(isrf_in, axis=(0, 1))
            resample_ids = adaptative_resampling(dw_in, mean_isrf,resampling_factor_list)
            new_dw = dw_in[resample_ids]
            new_isrf = isrf_in[:,:,resample_ids]
        elif dlambda:
            start = dw_in[0]
            end = dw_in[-1]
            new_dw = np.arange(start, end + dlambda, dlambda)
            new_isrf = da.full((1280, 15, new_dw.size), fill_value=np.nan)
            for i in range(1280):
                for j in range(15):
                    if not np.isnan(isrf_in[i, j, 0]):
                        new_isrf[i, j] = da.from_delayed(
                            delayed(np.interp)(new_dw, dw_in, isrf_in[i, j]),
                            (new_dw.size,),
                            dtype=np.float64,
                        )
            with ProgressBar():
                new_isrf = new_isrf.compute()

        nc_out.createDimension("dw1", new_dw.size)
        band1.createVariable("DeltaWavelength", np.float64, ("dw1",))
        band1["DeltaWavelength"][:] = new_dw

        band1.createVariable("ISRF", np.float64, ("x1", "w1", "dw1",))
        band1["ISRF"][:] = new_isrf

    print(f"New file written: {outfile}")


def main():
    parser = argparse.ArgumentParser(
        description="Code to resample ISRF tables either with resampling factors or by interpolating to a new wavenumber spacing"
    )
    parser.add_argument(
        "-i", "--infile", required=True, help="full path to input isrf file"
    )
    parser.add_argument(
        "-o", "--outfile", required=True, help="full path to the output isrf file"
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-f", "--resampling-factor", default=None, type=int, help="resampling factor"
    )
    group.add_argument(
        "-d", "--dlambda", default=None, type=float, help="new spacing in nm"
    )
    group.add_argument(
        "-m",
        "--multi-resampling",
        nargs=3,
        default=None,
        type=float,
        help="[fast,med,slow] resampling factors, fast will be used where the ISRF varies the fastest",
    )
    args = parser.parse_args()

    resample_isrf(
        args.infile,
        args.outfile,
        args.resampling_factor,
        args.dlambda,
        args.multi_resampling,
    )


if __name__ == "__main__":
    main()
