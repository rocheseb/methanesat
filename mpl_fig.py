import os
import pickle
import argparse


def open_pickled_fig(path: str):
    """
	Open a pickled matplotlib figure
	path: full path to the pickle file
	"""
    with open(path, "rb") as infile:
        fig = pickle.load(infile)
    fig.show()


def main():
    parser = argparse.ArgumentParser(description="Open a pickled matplotlib figure")
    parser.add_argument("path", help="full path to the pickle file")
    args = parser.parse_args()

    if not os.path.exists(args.path):
        raise Exception(f"Wong path: {args.path}")

    open_pickled_fig(args.path)


if __name__ == "__main__":
    main()
