from pylab import *
from compare_heatmaps import make_hist
from mair_interface import *
from netCDF4 import *
from mair_box import stored_boxes
import glob
from scipy.interpolate.interpnd import _ndim_coords_from_arrays
from contextlib import ExitStack
import warnings


def grid_prep(
    lon: Union[np.ndarray, da.core.Array],
    lat: Union[np.ndarray, da.core.Array],
    x: Union[np.ndarray, da.core.Array],
    lon_lim: Annotated[Sequence[float], 2],
    lat_lim: Annotated[Sequence[float], 2],
    res: float = 20,
    chunk_size: int = 100,
    method: str = "cubic",
) -> Annotated[Sequence[da.core.Array], 3]:
    """
    get a variable ready to plot with plt.pcolormesh(lon_grid,lat_grid,x_grid_avg)
    lon: the input longitudes with (along-track,across-track) dimensions
    lat: the input latitudes with (along-track,across-track) dimensions
    x: the input data to regrid with (along-track,across-track) dimensions
    lon_lim: the [min,max] of the longitudes to regrid the data on
    lat_lim: the [min,max] of the latitudes to regrid the data on
    res: the spatial resolution in meters    
    chunk_size: the size of along-track chunks to separate the data into
    method: griddata interpolation method        
    """
    if x.ndim != 2:
        raise Exception("grip_prep expects arrays with 2 dimensions")
    if not np.array_equal(x.shape, lat.shape) or not np.array_equal(x.shape, lon.shape):
        raise Exception("Shape mismatch between the inputs arrays")
    if res <= 1:
        raise Exception("The resolution cannot be less than 1 meters or negative")
    if chunk_size < 1:
        raise Exception("The along-track chunk size cannot be smaller than 1 pixel")

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        if type(lat) == da.core.Array:
            lat = lat.compute()
        if type(lon) == da.core.Array:
            lon = lon.compute()
        if type(x) == da.core.Array:
            x = x.compute()

    mid_lat = (np.nanmax(lat) - np.nanmin(lat)) / 2.0

    lat_res, lon_res = meters_to_lat_lon(res, mid_lat)

    nalong, nacross = lat.shape

    along_ids = np.arange(nalong)

    chunked_ids = list(chunked(along_ids, chunk_size))

    print(
        f"Calling grid_prep on {len(chunked_ids)} chunks of {chunk_size} along-track pixels"
    )

    lon_range = da.arange(lon_lim[0], lon_lim[1], lon_res)
    lat_range = da.arange(lat_lim[0], lat_lim[1], lat_res)

    # compute the lat-lon grid now so it doesn't have to be computed for each griddata call
    lon_grid, lat_grid = dask.compute(*da.meshgrid(lon_range, lat_range))
    regrid_points = _ndim_coords_from_arrays((lon_grid.ravel(), lat_grid.ravel()))
    x_grid_list = []
    run_times = []
    help_message = ""
    for i, ids_slice in enumerate(chunked_ids):
        start = time.time()
        if run_times:
            help_message = f" | min / mean / max runtime of chunks = {np.min(run_times):.1f} / {np.mean(run_times):.1f} / {np.max(run_times):.1f} seconds"
        sys.stdout.write(
            f"\rgrid_prep now doing chunk {i+1:>3}/{len(chunked_ids)}{help_message}"
        )
        sys.stdout.flush()

        lat_sliced = lat[ids_slice, :]
        lon_sliced = lon[ids_slice, :]
        x_sliced = x[ids_slice, :]

        x_sliced[x_sliced.mask] = np.nan
        lat_sliced[lat_sliced.mask] = np.nan

        nonan = np.logical_and(~np.isnan(x_sliced), ~np.isnan(lat_sliced))
        flat_x = x_sliced[nonan]
        flat_lat = lat_sliced[nonan]
        flat_lon = lon_sliced[nonan]

        if not flat_x.size:
            continue

        x_grid = griddata(
            (flat_lon, flat_lat),
            flat_x,
            (lon_grid, lat_grid),
            method=method,
            rescale=True,
        )

        cloud_points = _ndim_coords_from_arrays((flat_lon, flat_lat))
        tri = Delaunay(cloud_points)

        outside_hull = np.zeros(lon_grid.size).astype(bool)
        if method == "nearest":
            # filter out the extrapolated points when using nearest neighbors
            outside_hull = tri.find_simplex(regrid_points) < 0

        # filter out points that fall in large triangles
        # create a new scipy.spatial.Delaunay object with only the large triangles
        large_triangles = ~filter_large_triangles(cloud_points, tri)
        large_triangle_ids = np.where(large_triangles)[0]
        subset_tri = tri  # this doesn't preserve tri, effectively just a renaming
        # the find_simplex method only needs the simplices and neighbors
        subset_tri.nsimplex = large_triangle_ids.size
        subset_tri.simplices = tri.simplices[large_triangles]
        subset_tri.neighbors = tri.neighbors[large_triangles]
        # update neighbors
        for i, triangle in enumerate(subset_tri.neighbors):
            for j, neighbor_id in enumerate(triangle):
                if neighbor_id in large_triangle_ids:
                    subset_tri.neighbors[i, j] = np.where(
                        large_triangle_ids == neighbor_id
                    )[0]
                elif neighbor_id >= 0:
                    subset_tri.neighbors[i, j] = -1

        inside_large_triangles = (
            subset_tri.find_simplex(regrid_points, bruteforce=True) >= 0
        )
        invalid_slice = np.logical_or(outside_hull, inside_large_triangles)
        x_grid[invalid_slice.reshape(x_grid.shape)] = np.nan

        x_grid_list.append(x_grid)
        run_times.append(time.time() - start)

    stacked_grid = da.stack(x_grid_list, axis=0)
    x_grid_avg = da.nanmean(stacked_grid, axis=0)

    return lon_grid, lat_grid, x_grid_avg


def total_column(nc_in, gas, mode="ret"):
    """
    Compute total column of given gas

    mode: one of ["ret","prior"]
    """
    varlist = nc_in.variables
    if mode == "ret":
        prefix = ""
    elif mode == "prior":
        prefix = "prior_"
    pcol = f"{prefix}{gas}_pcol"
    mr = f"{prefix}{gas}_mr"
    if pcol in varlist:
        gas_pcol = da.from_array(nc_in[pcol])
        col = da.sum(gas_pcol, axis=0)
    elif mr in varlist and "air_pcol" in varlist:
        gas_mr = da.from_array(nc_in[mr])
        air_pcol = da.from_array(nc_in["air_pcol"])
        col = da.sum(gas_mr * air_pcol, axis=0)
    else:
        raise Exception(f"need either {pcol} or {mr} + air_pcol in {nc_in.filepath()}")

    return col


def mair_L3(
    ch4_file: str,
    o2_file: str,
    outfile: str,
    lon_lim: Annotated[Sequence[float], 2], # degrees east
    lat_lim: Annotated[Sequence[float], 2], # degrees north
    time_lim: Optional[Annotated[Sequence[datetime], 2]] = None,
    res: float = 20.0, # meters
    chunk_size: int = 100,
    method: str = "cubic",
    add_var_list: Optional[List] = None,
) -> None:

    if ch4_file is None and o2_file is None:
        raise Exception("need an input file")

    mid_lat = (lat_lim[1] - lat_lim[0]) / 2.0
    lat_res, lon_res = meters_to_lat_lon(res, mid_lat)
    lon_range = np.arange(lon_lim[0], lon_lim[1], lon_res)
    lat_range = np.arange(lat_lim[0], lat_lim[1], lat_res)

    nlat = lat_range.size
    nlon = lon_range.size

    with ExitStack() as stack:
        nc_o2 = stack.enter_context(Dataset(o2_file, "r"))
        nc_ch4 = stack.enter_context(Dataset(ch4_file, "r"))

        time_slice = ()  # empty tuple selects the full slice (x[:]==x[()])
        if time_lim is not None:
            o2_time = num2date(
                nc_o2["time"][
                    :, 600
                ],  # get an along-track row near the center of the cross-track (~600) to make sure we don't get nans
                units="hours since 1985-01-01",
                only_use_cftime_datetimes=False,
                only_use_python_datetimes=True,
            )
            time_slice = np.where((o2_time >= time_lim[0]) & (o2_time < time_lim[1]))[0]
            if time_slice.size == 0:
                raise Exception(
                    f"There is no data between {time_lim[0]} and {time_lim[1]}"
                )
            if time_slice.size == o2_time.size:
                time_slice = ()
            start = datetime.strftime(time_lim[0], "%Y%m%dT%H%M")
            end = datetime.strftime(time_lim[1], "%Y%m%dT%H%M")
            outfile = outfile.replace(".nc", f"_{start}_{end}.nc")

        if os.path.exists(outfile):
            write_mode = "r+"
        else:
            write_mode = "w"

        print(f"L3 data will be written in {outfile}")
        nc_out = stack.enter_context(Dataset(outfile, write_mode))

        if "longitude" not in nc_out.dimensions:
            nc_out.createDimension("longitude", nlon)
            nc_out.createVariable("longitude", np.float32, ("longitude",))
            nc_out["longitude"][:] = lon_range
        if "latitude" not in nc_out.dimensions:
            nc_out.createDimension("latitude", nlat)
            nc_out.createVariable("latitude", np.float32, ("latitude",))
            nc_out["latitude"][:] = lat_range

        lat = nc_o2["latitude"][time_slice]
        lon = nc_o2["longitude"][time_slice]

        with warnings.catch_warnings():
            warnings.simplefilter("ignore")

            o2_col = total_column(nc_o2, "o2")[time_slice]
            air_col_o2 = total_column(nc_o2, "air")[time_slice]

            ch4_slice_ids = da.from_array(nc_o2["ch4_slice_ids"])[time_slice]

            prior_ch4_col = total_column(nc_ch4, "prior_ch4")[ch4_slice_ids]
            prior_h2o_col = total_column(nc_ch4, "prior_h2o")[ch4_slice_ids]
            prior_air_col_ch4 = total_column(nc_ch4, "prior_air")[ch4_slice_ids]
            ch4_col = total_column(nc_ch4, "ch4")[ch4_slice_ids]
            co2_col = total_column(nc_ch4, "co2")[ch4_slice_ids]
            air_col_ch4 = total_column(nc_ch4, "air")[ch4_slice_ids]
            h2o_col = total_column(nc_ch4, "h2o")[ch4_slice_ids]            

            dry_air_col_o2 = air_col_o2 - h2o_col
            dry_air_col_ch4 = air_col_ch4 - h2o_col
            prior_dry_air_col_ch4 = prior_air_col_ch4 - prior_h2o_col

            prior_xch4 = prior_ch4_col / prior_dry_air_col_ch4

            xch4_o2proxy = 0.2095 * ch4_col / o2_col
            xco2_o2proxy = 0.2095 * co2_col / o2_col
            xco2_ch4proxy = prior_xch4 * co2_col / ch4_col
            xco2 = co2_col / dry_air_col_ch4
            xch4_co2proxy = da.from_array(nc_ch4["xch4_co2proxy"])[ch4_slice_ids]

        var_dict = {
            "xch4_co2proxy": {
                "val": xch4_co2proxy * 1e9,
                "description": "prior_xco2 * ch4_column / co2_column",
                "units": "ppb",
            },
            "xch4_o2proxy": {
                "val": xch4_o2proxy * 1e9,
                "description": "0.2095 * ch4_column / o2_column",
                "units": "ppb",
            },
            "xco2_o2proxy": {
                "val": xco2_o2proxy * 1e6,
                "description": "0.2095 * co2_column / o2_column",
                "units": "ppm",
            },
            "xco2_ch4proxy": {
                "val": xco2_ch4proxy * 1e6,
                "description": "prior_xch4 * co2_column / ch4_column",
                "units": "ppm",
            },
            "xco2": {
                "val": xco2 * 1e6,
                "description": "co2_column / dry_air_column with dry_air_column derived from the CH4 window",
                "units": "ppm",
            },
            "air_ratio": {
                "val": dry_air_col_ch4 / dry_air_col_o2,
                "description": "dry air column derived from the CH4 window divided by that derived from the O2 window",
                "units": "",
            },
            "dp": {
                "val": da.from_array(nc_o2["ret_state"])[4][time_slice]
                - da.from_array(nc_o2["prior_state"])[4][time_slice],
                "description": "ret_psurf-prior_psurf from the O2 window",
                "units": "hPa",
            },
            "psurf_dofs": {
                "val": da.from_array(nc_o2["psurf_dofs"])[time_slice],
                "description": "Surface pressure degrees of freedom for signal",
                "units": "",
            },
            "o2_column":{
                "val": o2_col,
                "description": "Total column of O2",
                "units": "molecules/cm^2",
            },
            "ch4_column":{
                "val": ch4_col,
                "description": "Total column of CH4",
                "units": "molecules/cm^2",
            },
            "co2_column":{
                "val": co2_col,
                "description": "Total column of CO2",
                "units": "molecules/cm^2",
            },
            "dry_air_column":{
                "val": dry_air_col_o2,
                "description": "Total column of dry air derived from the O2 window",
                "units": "molecules/cm^2",
            },            
        }

        if add_var_list is not None:
            for var in add_var_list:
                if var not in var_dict:
                    print(f"{var} is not in var_dict")

            var_dict = {
                key: val for key, val in var_dict.items() if key in add_var_list
            }

        make_hist_plots = False
        if make_hist_plots:
            fig_co2,axco2 = subplots()
            fig_ch4,axch4 = subplots()
            for fig in [fig_ch4,fig_co2]:
                fig.set_size_inches(8,8)
            title = f"rf04_release_1x1_L2_{start}_{end}"
            axch4.set_title(title)
            axco2.set_title(title)
        valid_xtrack = np.where(~lon[0].mask)[0]
        colors = {
            "xco2":"C0",
            "xco2_o2proxy":"C1",
            "xco2_ch4proxy":"C2",
            "xch4_co2proxy":"C0",
            "xch4_o2proxy":"C1",
        }
        for var in var_dict:

            print(f"\nNow doing {var}")

            x = var_dict[var]["val"]

            if make_hist_plots:
                if len(x.shape) == 3:
                    print(f"Summing {var} along first dimension")
                    x = da.sum(x, axis=0)
                
                if var.startswith('xco2'):
                    ax = axco2
                    rng = [340,480]
                    ax.set_xlabel("$XCO_2$ (ppm)")
                elif var.startswith('xch4'):
                    ax = axch4
                    rng = [1500,2200]
                    ax.set_xlabel("$XCH_4$ (ppb)")
                else:
                    continue
                make_hist(ax,x[:,valid_xtrack].flatten().compute(),label=var,nbins=100,rng=rng,color=colors[var])
                continue
            lon_grid, lat_grid, x_grid = grid_prep(
                lon, lat, x, lon_lim, lat_lim, res, chunk_size, method
            )

            if var not in nc_out.variables:
                nc_out.createVariable(var, np.float32, ("longitude", "latitude"))
                nc_out[var].description = var_dict[var]["description"]
                nc_out[var].units = var_dict[var]["units"]
            nc_out[var][:] = x_grid.T

            if "longitude_grid" not in nc_out.variables:
                nc_out.createVariable(
                    "longitude_grid", np.float32, ("longitude", "latitude")
                )
                nc_out["longitude_grid"][:] = lon_grid.T
                nc_out.createVariable(
                    "latitude_grid", np.float32, ("longitude", "latitude")
                )
                nc_out["latitude_grid"][:] = lat_grid.T
        print(f"\n{outfile}")
        if make_hist_plots:
            fig_co2.savefig(os.path.join('/n/home11/sroche/testing/plots/RF04_release_L2/co2',f"{title}_xco2_hist.png"))
            fig_ch4.savefig(os.path.join('/n/home11/sroche/testing/plots/RF04_release_L2/ch4',f"{title}_xch4_hist.png"))


def main():
    parser = argparse.ArgumentParser(description="Regrid L2 data")
    parser.add_argument(
        "--ch4-file",
        help="full path to the file containing combined CH4 band L2 outputs",
    )
    parser.add_argument(
        "--o2-file",
        help="full path to the file containing combined O2 band L2 outputs",
    )
    parser.add_argument(
        "-o", "--outfile", help="full path to the output L3 file",
    )
    parser.add_argument(
        "--lon-lim",
        nargs=2,
        type=float,
        default=None,
        help="min and max longitude of the L3 data in degrees east",
    )
    parser.add_argument(
        "--lat-lim",
        nargs=2,
        type=float,
        default=None,
        help="min and max latitude of the L3 data in degrees north",
    )
    parser.add_argument(
        "--time-lim",
        nargs=2,
        type=str,
        default=None,
        help="min and max time in yyyymmdd_HHMM format",
    )
    parser.add_argument(
        "-b",
        "--box-name",
        help="box name, if given use the lon_lim and lat_lim saved in a dictionary",
    )
    parser.add_argument(
        "--method",
        default="cubic",
        choices=["cubic", "linear", "nearest"],
        help="the interpolation method for scipy.interpolate.griddata",
    )
    parser.add_argument(
        "-r",
        "--resolution",
        default=20,
        type=float,
        help="the regridding resolution in meters",
    )
    parser.add_argument(
        "--chunk-size",
        default=100,
        type=int,
        help="along-track chunk size for the gridding",
    )
    parser.add_argument(
        "--add",
        nargs="*",
        default=None,
        type=str,
        help="list of variables to append to existing file (must be in var_dict)",
    )
    args = parser.parse_args()

    for infile in [args.ch4_file, args.o2_file]:
        if infile and not os.path.isfile(infile):
            raise Exception(f"Wrong path: {infile}")
    outdir = os.path.dirname(args.outfile)
    if not os.path.isdir(outdir):
        raise Exception(f"Wrong path: {outdir}")

    if not args.outfile.endswith(".nc"):
        raise Exception(f"the output L3 file must be a .nc file")

    if args.time_lim is not None:
        time_lim = [datetime.strptime(i, "%Y%m%d_%H%M") for i in args.time_lim]
    else:
        time_lim = None

    if args.box_name:
        args.lon_lim = stored_boxes[args.box_name]["xlim"]
        args.lat_lim = stored_boxes[args.box_name]["ylim"]

    mair_L3(
        args.ch4_file,
        args.o2_file,
        args.outfile,
        args.lon_lim,
        args.lat_lim,
        time_lim,
        args.resolution,
        args.chunk_size,
        args.method,
        args.add,
    )


if __name__ == "__main__":
    main()
