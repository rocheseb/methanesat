import os
import glob
from compare_heatmaps import *
from netCDF4 import Dataset
import numpy as np
import pandas as pd
import multiprocessing
import tqdm
import argparse
import warnings


def get_window_ids(infile,nu0=1629,nuf=1654):
    with Dataset(infile,'r') as nc:
        wvl = nc['Band1/Wavelength'][:]

    for i in range(wvl.shape[2]):
        if not wvl[:,:,i].mask.all():
            break
    for j in range(wvl.shape[2])[::-1]:
        if not wvl[:,:,i].mask.all():
            break       
    med_wvl = np.nanmedian(wvl[:,:,i:j+1],axis=(1,2))
    window_ids = np.where((nu0<=med_wvl) & (med_wvl<=nuf))[0]

    return window_ids


def compute_snr(infile,window_ids):

    if not list(window_ids):
        return np.full((rad.shape[1],rad.shape[2]),np.nan)

    with Dataset(infile,'r') as nc:
        rad = nc['Band1/Radiance'][:]
        unc = nc['Band1/RadianceUncertainty'][:]

    snr = np.nanmedian(rad[:,:,window_ids],axis=0)/np.nanmedian(unc[:,:,window_ids],axis=0)
    
    return snr


def snr_diagnostics(infile,window_ids):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        try:
            snr = compute_snr(infile,window_ids)
            result = np.array([infile,np.nanmin(snr),np.nanmax(snr),np.nanmedian(snr),np.nanmean(snr),np.nanstd(snr,ddof=1)])
        except:
            result = np.array([infile,np.nan,np.nan,np.nan,np.nan,np.nan])
    return result


def snr_diagnostics_star(args):
    return snr_diagnostics(*args)


def check_flight_snr(l1dir,srchstr="MethaneAIR*.nc",nu0=1629,nuf=1654,save_dir="",save_name="MAIR",ncores=None):
    l1_file_list = glob.glob(os.path.join(l1dir,srchstr))
    print(f"Found {len(l1_file_list)} L1 files")

    # Get Cores
    if ncores is None:
        ncores = int(np.ceil(multiprocessing.cpu_count()))
    else:
        if ncores > multiprocessing.cpu_count():
            raise Exception("You asked for more cores than you have!")

    # get the window indices along the wavelength dimension from the first granule
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        window_ids = get_window_ids(l1_file_list[0],nu0=nu0,nuf=nuf)

    print(f"Start parallel processing on {ncores} cores")
    with multiprocessing.Pool(ncores) as pp:
        args = ((l1_file,window_ids) for l1_file in l1_file_list)
        snr_diag = pd.DataFrame(tqdm.tqdm(pp.imap_unordered(snr_diagnostics_star,args),total=len(l1_file_list)),
                                columns=["l1_file","min_snr","max_snr","median_snr","mean_snr","std_snr"],
                                )
        pp.close()
        pp.join()
    snr_diag.loc[:,"min_snr":] = snr_diag.loc[:,"min_snr":].astype(np.float64)
    snr_diag["datetime"] = snr_diag["l1_file"].apply(lambda x: pd.to_datetime(os.path.basename(x).split('_')[3]))
    
    if save_dir:
        data_file = os.path.join(save_dir,f"{save_name}_snr_diag.csv")
        snr_diag.to_csv(data_file)
        print(data_file)       

    fig,ax = plt.subplots(1,2)
    fig.set_size_inches(12,6)
    make_hist(ax[0],np.array(snr_diag["min_snr"]).astype(np.float64),label="min",color="blue",rng=[0,250],nbins=100,exp_fmt=False)
    make_hist(ax[0],np.array(snr_diag["max_snr"]).astype(np.float64),label="max",color="red",rng=[0,250],nbins=100,exp_fmt=False)
    make_hist(ax[0],np.array(snr_diag["median_snr"]).astype(np.float64),label="median",color="purple",rng=[0,250],nbins=100,exp_fmt=False)
    make_hist(ax[0],np.array(snr_diag["mean_snr"]).astype(np.float64),label="mean",color="green",rng=[0,250],nbins=100,exp_fmt=False)
    make_hist(ax[0],np.array(snr_diag["std_snr"]).astype(np.float64),label="std",color="black",rng=[0,250],nbins=100,exp_fmt=False)
    ax[0].set_xlabel(f"{nu0}-{nuf} nm SNR")

    snr_diag.loc[:,"min_snr":].sort_values(by=['datetime']).plot('datetime',marker=0,markersize=1,linewidth=0,ax=ax[1],color=["blue","red","purple","green","black"])
    ax[1].set_ylim(-10,250)
    ax[1].set_ylabel(f"{nu0}-{nuf} nm SNR")
    plt.gcf().autofmt_xdate()
    fig.suptitle(save_name)
    plt.tight_layout()

    if save_dir:
        plot_file = os.path.join(save_dir,f"{save_name}_snr_diag.png")
        fig.savefig(plot_file)
        print(plot_file)


def main():
    parser = argparse.ArgumentParser(description="Check the snr of granules in a given flight")
    parser.add_argument("l1dir",help="full path to the directory with the L1 files")
    parser.add_argument("--save-name",help="name to use in the output files, only used when --save-dir is given")
    parser.add_argument("-s","--save-dir",default="",help="full path to the directory where plots and summary .csv will be saved")
    parser.add_argument("--srchstr",default="MethaneAIR*.nc",help="search string for selecting files")
    parser.add_argument("-w","--wvl",nargs=2,type=float,default=[1629,1654],help="wavelength limits for which the SNR will be calculated")
    parser.add_argument("-n","--ncores",default=None,type=int,help="number of cores to use")
    args = parser.parse_args()

    check_flight_snr(args.l1dir,srchstr=args.srchstr,nu0=args.wvl[0],nuf=args.wvl[1],save_dir=args.save_dir,save_name=args.save_name,ncores=args.ncores)


if __name__=="__main__":
    main()
