import os
import dask
from dask.diagnostics import ProgressBar
import argparse

@dask.delayed
def remove_file(f):
	os.remove(f)

def cleanup_l2(l2dir):
	flist = []
	yaml_file = os.path.join(l2dir,"job_process_list.yaml")
	if os.path.exists(yaml_file):
		flist += [yaml_file]
	flist += [os.path.join(l2dir,"logs",i) for i in os.listdir(os.path.join(l2dir,"logs"))]
	flist += [os.path.join(l2dir,"control",i) for i in os.listdir(os.path.join(l2dir,"control"))]
	flist += [os.path.join(l2dir,i) for i in os.listdir(l2dir) if i.startswith("splat_commands_")]

	print(f"Will clean up {len(flist)} files")

	result = []
	for f in flist:
		result.append(remove_file(f))

	with ProgressBar():
		dask.compute(*result)


def main():
	parser = argparse.ArgumentParser(description="Remove splat_commands_*.txt files, the 'logs' and 'control' folders, and the job_process_list.yaml file")
	parser.add_argument("l2dir",help="full path to the level2 output directory")
	args = parser.parse_args()

	cleanup_l2(args.l2dir)

if __name__=="__main__":
	main()
