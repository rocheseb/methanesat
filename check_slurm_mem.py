import os
import sys
import argparse
from netCDF4 import *
from pylab import *
from compare_heatmaps import make_hist
import numpy as np
import pandas as pd
import dask
import dask.array as da
from dask import delayed
from dask.diagnostics import ProgressBar
from subprocess import PIPE, run


units_map = {"GB":1.0,"MB":1e-3,"KB":1e-6}


@dask.delayed
def check_mem(job_id):
	"""
	Check the memory usage of a slurm job
	"""
	out = run(f"seff {job_id} | grep 'Memory Utilized'",stdout=PIPE,stderr=PIPE,shell=True)
	if len(out.stdout.decode().split())!=4:
		return np.nan
	mem = float(out.stdout.decode().split()[2])
	scale = units_map[out.stdout.decode().split()[3]]
	return mem*scale
	

def check_slurm_mem(user,date,srchstr,show_sacct=False):
	"""
	Check the memory usage of all slurm jobs after date
	"""
	out = run(f"sacct -S {date} -u {user} --format='JobID%30,JobName%15,Partition%20,State%15,Start%20,End%20' | grep {srchstr} | grep COMPLETED",stdout=PIPE,stderr=PIPE,shell=True)

	if show_sacct:
		print(out.stdout.decode())

	splitlines = np.array([line.split() for line in out.stdout.decode().split('\n')[:-1]])
	job_ids = np.array([line[0] for line in splitlines])
	start = np.array([pd.Timestamp(str(line[-2])) for line in splitlines])
	end = np.array([pd.Timestamp(str(line[-1])) for line in splitlines])
	runtime = np.array([(end[i]-start[i]).total_seconds() for i in range(len(start))])/3600.0

	result = []
	for job_id in job_ids:
		result.append(check_mem(job_id))
	
	with ProgressBar():
		result = np.array(dask.compute(*result))

	ids = np.argsort(result)
	content = [f"{job_ids[i]}: {result[i]} GB\n" for i in ids]
	with open("slurm_mem_use.out","w") as outfile:
		outfile.writelines(content)
	print("slurm_mem_use.out")

	print(f"{len(result)} jobs")
	print(f"Mean memory usage: {np.nanmean(result)} GB")
	print(f"Median memory usage: {np.nanmedian(result)} GB")
	print(f"Max memory usage: {np.nanmax(result)} GB")
	print(f"\nMean runtime: {np.nanmean(runtime)} hours")
	print(f"Min runtime: {np.nanmin(runtime)} hours")
	print(f"Max runtime: {np.nanmax(runtime)} hours")

	fig,ax = subplots(1,2)
	ax[0].set_xlabel("Memory Usage (GB)")
	ax[1].set_xlabel("Runtime (hours)")
	make_hist(ax[0],result,"Memory Usage (GB)","black",[0,15],100,exp_fmt=False)
	make_hist(ax[1],runtime,"Runtime (hours)","black",[0,480],200,exp_fmt=False)
	show()


def main():
	parser = argparse.ArgumentParser(description="check the slurm job history to get an estimate of the memory usage of jobs")
	parser.add_argument("date",help="starting date for checking the history in format YYYY-MM-DD")
	parser.add_argument("srchstr",help="search pattern for checking the history, either a job_id or job name would work")
	parser.add_argument("-u","--user",default=os.environ["USER"],help="username to check for in the search")
	parser.add_argument("--show",action="store_true",help="if given, print the sacct output")
	args = parser.parse_args()

	check_slurm_mem(args.user,args.date,args.srchstr,show_sacct=args.show)


if __name__=="__main__":
	main()
