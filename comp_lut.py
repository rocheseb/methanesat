# coding: utf-8
from pylab import *
import numpy as np
import os
import argparse
import sys
from netCDF4 import *
sys.path.insert(0,'/n/home11/sroche/gitrepos/sci-level2-splat/pysplat/pysplat')
from generate_lut import *

def comp_lut(gas,lut1_file,lut2_file,i,j,k,wlim,max_iso):
    iso_data = {key:val for key,val in read_isotopologs().items() if val['gas']==gas}
    niso = len(iso_data.keys())
    if gas.lower()=='O2' and '0{}'.format(gas.lower()) in iso_data.keys():
        niso = niso-1
    molec_id = iso_data[list(iso_data.keys())[0]]['kgas']

    if max_iso and max_iso<niso:
        niso = max_iso

    db = None
    if gas in ['O2','CO2','CH4']:
        db = read_hitran_db(f'/n/home11/sroche/testing/HITRAN/{gas.upper()}.data')

    with Dataset(lut1_file,'r') as lut1, Dataset(lut2_file,'r') as lut2:
        fig,ax = subplots(1,2,sharex=True)
        wl1 = lut1['Wavelength'][:]
        wl2 = lut2['Wavelength'][:]
        wn1 = 1e7/wl1
        wn2 = 1e7/wl2
        ax[0].plot(wn1,lut1['CrossSection'][i,j,k],marker='o',markersize=2,linewidth=0,c='black',label='{} (A)'.format(os.path.basename(lut1_file)))
        ax[0].plot(wn2,lut2['CrossSection'][i,j,k],marker='o',markersize=2,linewidth=0,c='red',label='{} (B)'.format(os.path.basename(lut2_file)))
        ax[0].legend()
        ax[0].set_title('p={} hPa; T={} K; $H_2O$={:.3f}'.format(lut1['Pressure'][i]/100,lut1['Temperature'][i,j],lut1['Broadener_01_VMR'][k]))
        if wn1[0]==wn2[0] and wn1[-1]==wn2[-1]:
            ax[1].plot(wn1,lut1['CrossSection'][i,j,k]/lut2['CrossSection'][i,j,k],markersize=1,marker='o',linewidth=0)
        ax[1].set_title('A/B ratio')
        ax[0].set_ylabel('Absorption Cross Section ($cm^2.molec^{-1}$)')
        for iax in ax:
            iax.grid()
            iax.set_xlabel('Wavenumber ($cm^{-1}$)')
        #if db is not None:
        #    for iiso in range(1,niso):
        #        ids = np.where((db['nu']>wlim[0]) & (db['nu']<wlim[1]) & (db['molec_id']==molec_id) & (db['iso']==iiso))[0]
        #        dots, = ax[0].plot(db['nu'][ids],np.zeros(ids.size),marker='o',linewidth=0)
        #        ax[1].plot(db['nu'][ids],np.ones(ids.size),marker='o',linewidth=0,label=f'{iiso}{gas.lower()}',c=dots.get_color())
        #    ax[1].legend()
        ax[1].set_ylim(0,1.05)
        show()

def main():
    parser = argparse.ArgumentParser(description="Plot cross-sections from two different LUT as well as their ratio for a given set of T,P, and broadener vmr")
    parser.add_argument('lut1',help='full path to numerator linelist')
    parser.add_argument('lut2',help='full path to denominator linelist')
    parser.add_argument('-t',type=int,default=10,help='temperature index (along the LUT temperature dimension)')
    parser.add_argument('-p',type=int,default=-1,help='pressure index (along the LUT pressure dimension)')
    parser.add_argument('-b',type=int,default=1,help='broadener vmr index (along the LUT Broadener_01_VMR dimension)')
    parser.add_argument('-g','--gas',default='O2',help='molecule name')
    parser.add_argument('-l','--wlim',nargs=2,default=[7885-120,7885+120],type=float,help='window limits in wavenumbers (cm-1)')
    parser.add_argument('-i','--max-iso',default=None,type=int,help='limit the maximum number of isotopologs to be marked by dots')

    args = parser.parse_args()

    comp_lut(args.gas,args.lut1,args.lut2,args.p,args.t,args.b,args.wlim,args.max_iso)

if __name__=="__main__":
    main()
