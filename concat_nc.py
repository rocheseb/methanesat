import os
import sys
from typing import Optional, Sequence, Tuple, Union, Annotated, List, Generator
import glob
import argparse
from contextlib import ExitStack
from datetime import datetime
import netCDF4
from netCDF4 import *
import numpy as np
import pandas as pd
import dask
import dask.array as da
from dask.diagnostics import ProgressBar


common_var_dict = {
    "Level1/Latitude": "latitude",
    "Level1/Longitude": "longitude",
    "Level1/CornerLatitudes": "corner_latitude",
    "Level1/CornerLongitudes": "corner_longitude",
    "Level1/SolarZenithAngle": "sza",
    "Level1/ViewingZenithAngle": "vza",
    "Level1/SurfaceAltitude": "zsurf",
    "Level1/Time": "time",
    "Profile/AirPartialColumn": "prior_air_pcol",
    "Profile/O2_GasPartialColumn": "prior_o2_pcol",
    "Profile/H2O_GasPartialColumn": "prior_h2o_pcol",
    "Profile/H2O_GasMixingRatio": "prior_h2o_mr",  ##
    "Posteriori_Profile/AirPartialColumn": "air_pcol",
    "SpecFitDiagnostics/NumberOfIterations": "iterations",
    "SpecFitDiagnostics/FitResidualRMS": "rms_resid",
    "SpecFitDiagnostics/CO2_ScaleFactorDoFS": "co2_dofs",
    "SpecFitDiagnostics/TemperatureShift_DoFS": "tshift_dofs",
    "SpecFitDiagnostics/SurfacePressure_DoFS": "psurf_dofs",
    "SpecFitDiagnostics/H2O_ScaleFactorDoFS": "h2o_dofs",
    "SpecFitDiagnostics/ISRFParameterCoeff_Window1_DoFS": "isrfsqz_w1_dofs",
    "SpecFitDiagnostics/APrioriState": "prior_state",
    "SpecFitDiagnostics/APosterioriState": "ret_state",
    # "SpecFitDiagnostics/APrioriCovariance": "prior_cov",
    # "SpecFitDiagnostics/APosterioriCovariance": "ret_cov",
    # "Profile/AirMolecularWeight": "prior_molecular_weight",
    # "Profile/Gravity": "prior_gravity",
    # "Posteriori_Profile/AirMolecularWeight": "molecular_weight",
    # "Posteriori_Profile/Gravity": "gravity",
}

ch4_var_dict = {
    "Profile/CH4_GasPartialColumn": "prior_ch4_pcol",
    "Profile/CO2_GasPartialColumn": "prior_co2_pcol",
    "Posteriori_Profile/CH4_GasMixingRatio": "ch4_mr",
    "Posteriori_Profile/CO2_GasMixingRatio": "co2_mr",
    "Posteriori_Profile/O2_GasMixingRatio": "o2_mr",
    "Posteriori_Profile/H2O_GasMixingRatio": "h2o_mr",
    "Posteriori_Profile/CH4_ProxyMixingRatio": "xch4_co2proxy",
    "SpecFitDiagnostics/CH4_ScaleFactorDoFS": "ch4_dofs",
    "SpecFitDiagnostics/ISRFParameterCoeff_Window2_DoFS": "isrfsqz_w2_dofs",
}

o2_var_dict = {
    "Posteriori_Profile/H2O_GasPartialColumn": "h2o_pcol",
    "Posteriori_Profile/O2_GasPartialColumn": "o2_pcol",
    "SpecFitDiagnostics/PA1_ScaleFactorDoFS": "cia_dofs",
    "Posteriori_Profile/PressureEdge": "pressure_edge",
}

common_var_dict = {
    "Level1/Latitude": "latitude",
    "Level1/Longitude": "longitude",
    "Level1/SolarZenithAngle": "sza",
    "Level1/ViewingZenithAngle": "vza",
    "Level1/Time": "time",
    "SpecFitDiagnostics/APrioriState": "prior_state",
    "SpecFitDiagnostics/APosterioriState": "ret_state",    
}
o2_var_dict = {
    "Posteriori_Profile/H2O_GasPartialColumn": "h2o_pcol",
    "Posteriori_Profile/O2_GasPartialColumn": "o2_pcol",
}

def get_sv_info(dset):
    """
    Get the state vector index for the given variable
    var: complete state vector variable name
    """
    sv_dict = dset["SpecFitDiagnostics"]["APosterioriState"].__dict__

    sv_info = {}

    for key, val in sv_dict.items():
        if key.startswith("SubStateName"):
            sv_info[key] = {"name": sv_dict[key]}
            num = int(key.split("_")[-1]) - 1
            sv_info[key]["slice"] = np.array(
                [sv_dict["SubState_Idx0"][num] - 1, sv_dict["SubState_Idxf"][num]]
            )
            sv_info[key]["isprofile"] = sv_dict["SubState_IsProfile"][num]
            sv_info[key]["ischebyshev"] = sv_dict["SubState_IsChebyshev"][num]
            sv_info[key]["wvl0"] = sv_dict["SubState_Wvl0"][num]
            sv_info[key]["wvlf"] = sv_dict["SubState_Wvlf"][num]

    return sv_info


def check_valid_xtrack(infile: str) -> None:
    """
    Check for rows of NaNs in the concatenated file
    """

    with Dataset(infile, "r") as nc:
        valid_xtrack_ids = np.where([~i.mask.all() for i in nc["latitude"][:].T])[0]
        valid_range = np.arange(valid_xtrack_ids[0], valid_xtrack_ids[-1] + 1)
        if valid_xtrack_ids.size != valid_range.size:
            print(
                f"Some cross track rows are filled with masks: {set(valid_range).difference(valid_xtrack_ids)}"
            )
        else:
            print("There are no rows filled with masks")


def get_chunked_slices(n: int, chunksize: int):
    """
    Get chunked slices of indices to slice an array of size n with chunks of size chunk_size
    """
    ids = np.arange(n)
    chunked_ids = [ids[i : i + chunksize + 1] for i in range(0, len(ids), chunksize)]
    chunked_slices = np.array([slice(i[0], i[-1]) for i in chunked_ids])
    return chunked_slices


def concat_nc(
    indir: str,
    search: str,
    outfile: str,
    align_file: str,
    add_var_list: Optional[Sequence[str]] = None,
) -> None:
    """
    Concatenate a set of L2 files into a single netcdf file without groups and only a subset of variables
    indir: full path to the directory containing L2 files
    search: search pattern for file selection
    outfile: full path to the output netcdf file
    """
    if not os.path.exists(indir):
        raise Exception(f"Wrong path: {indir}")
    if not os.path.exists(os.path.dirname(outfile)):
        raise Exception(f"Wrong path: {os.path.dirname(outfile)}")

    file_path_list = np.array(glob.glob(os.path.join(indir, search)))
    if file_path_list.size == 0:
        raise Exception(f"No files matching {search} in {indir}")

    # set the variable name mapping
    var_dict = common_var_dict
    if "_O2_" in os.path.basename(file_path_list[0]):
        var_dict.update(o2_var_dict)
    else:
        var_dict.update(ch4_var_dict)

    if add_var_list is not None:
        var_dict = {key: val for key, val in var_dict.items() if key in add_var_list}
        if not var_dict:
            raise Exception(
                "None of the variables in add_var_list exist in the var_dict dictionary"
            )

    # make sure files are sorted
    dates = np.array(
        [
            datetime.strptime(
                os.path.basename(file_path).split("_")[3], "%Y%m%dT%H%M%S"
            )
            for file_path in file_path_list
        ]
    )
    ids = np.argsort(dates)
    file_path_list = list(file_path_list[ids])
    nfiles = len(file_path_list)

    if align_file:
        # get corresponding l1 file names to use the alignment file
        l1_file_name_list = [
            os.path.basename(file_path).replace("_CO2proxy.nc", ".nc")
            for file_path in file_path_list
        ]

        align_data = pd.read_csv(align_file)
        rm_count = 0
        # get rid of the align data that is not part of the scene
        align_data = align_data.loc[
            [fname in l1_file_name_list for fname in align_data["fname"]]
        ]

        # get rid of the flagged data
        align_data = align_data.loc[align_data["flag"] == 1].reset_index()

        # get rid of the granules that are not in the filtered align data
        atrack_slice_ids = []
        atrack_cumul = 0
        for i, file_name in enumerate(l1_file_name_list):
            with Dataset(
                os.path.join(indir, file_name.replace(".nc", "_CO2proxy.nc")), "r"
            ) as nc_dset:
                natrack = nc_dset.dimensions["jmx"].size
                atrack_slice_ids.append(np.arange(atrack_cumul, atrack_cumul + natrack))
                atrack_cumul += natrack
            if file_name not in align_data["fname"].values:
                file_path_list.remove(
                    os.path.join(indir, file_name.replace(".nc", "_CO2proxy.nc"))
                )
                rm_count += 1
                atrack_slice_ids = atrack_slice_ids[:-1]
        atrack_slice_ids = np.concatenate(
            atrack_slice_ids, axis=0
        )  # will be used to slice the concatenated ch4 file

        if rm_count:
            print(f"{rm_count} / {nfiles}  files not used due to bad alignment flag")
            print(
                f"A total of {align_data['offset_along'].round().sum()} cross-track columns will be lost due to along-track offsets"
            )

    print(f"Will combine {len(file_path_list)} files")

    atrack_tot = 0
    xtrack_offset_list = []
    atrack_offset_list = []
    atrack_start_id_list = []
    print("Initial setup")
    # get the total number of along track pixels and fill the offset lists defined above, will be used in the main loop
    missing_variables = []
    for dset_id, path in enumerate(file_path_list):
        with Dataset(path) as nc_dset:
            atrack_start_id_list.append(atrack_tot)
            if dset_id == 0:
                xtrack_tot = nc_dset.dimensions["imx"].size
                nlev = nc_dset.dimensions["lmx"].size
                nstate = nc_dset.dimensions["xmx"].size
                rm_keys = []
                for key, val in var_dict.items():
                    try:
                        check = nc_dset[key]
                    except (IndexError, KeyError):
                        missing_variables.append(key)
                        rm_keys.append(key)
                        continue
                for key in rm_keys:
                    del var_dict[key]
            atrack_tot += nc_dset.dimensions["jmx"].size

            file_name = os.path.basename(nc_dset.filepath()).replace("_CO2proxy", "")
            if align_file and file_name.startswith("MethaneAIR_L1B_O2"):
                xtrack_offset_list.append(
                    int(round(align_data.loc[dset_id]["offset_across"]))
                )
                atrack_offset_list.append(
                    int(round(align_data.loc[dset_id]["offset_along"]))
                )
    if missing_variables:
        print(
            "Could not find the following variables:\n" + "\n".join(missing_variables)
        )

    xtrack_chunksize = 80
    # try to define data chunks to speed up the writing to netcdf
    xtrack_chunked_slices = get_chunked_slices(xtrack_tot, xtrack_chunksize)

    out_mode = "w"
    if add_var_list is not None:
        out_mode = "r+"
    with ExitStack() as stack:
        print("Opening datasets ...")
        # the opening can take a while for many files
        nc_dset_list = [
            stack.enter_context(Dataset(path, "r")) for path in file_path_list
        ]

        nc_out = stack.enter_context(Dataset(outfile, out_mode))

        # to --add variables with dimensions that were not initially implemented
        if (out_mode == "r+") and ("corner" not in nc_out.dimensions):
            nc_out.createDimension("corner", 4)
        if (out_mode == "r+") and ("layer_edge" not in nc_out.dimensions):
            nc_out.createDimension("layer_edge", nlev + 1)
        if (out_mode == "r+") and ("substate" not in nc_out.dimensions):
            nc_out.createDimension("substate", nsubstate)
        if (out_mode == "r+") and ("slice" not in nc_out.dimensions):
            nc_out.createDimension("slice", 2)

        sv_info = get_sv_info(nc_dset_list[0])
        nsubstate = len(sv_info.keys())

        if out_mode == "w":
            nc_out.createDimension("atrack", atrack_tot)
            nc_out.createDimension("xtrack", xtrack_tot)
            nc_out.createDimension("level", nlev)
            nc_out.createDimension("layer_edge", nlev + 1)
            nc_out.createDimension("state", nstate)
            nc_out.createDimension("substate", nsubstate)
            nc_out.createDimension("slice", 2)
            nc_out.createDimension("corner", 4)
            if align_file:
                nc_out.createVariable("ch4_slice_ids", np.int16, ("atrack"), zlib=True)
                nc_out["ch4_slice_ids"][:] = atrack_slice_ids

            # write the state vector slicing info
            nc_out.createVariable("substate_name", str, ("substate"))
            nc_out["substate_name"][:] = np.array(
                [sv_info[key]["name"] for key in sv_info]
            )

            nc_out.createVariable("substate_slice", np.int16, ("substate", "slice"))
            nc_out["substate_slice"][:] = np.array(
                [sv_info[key]["slice"] for key in sv_info]
            )

            nc_out.createVariable("substate_isprofile", np.int16, ("substate"))
            nc_out["substate_isprofile"][:] = np.array(
                [sv_info[key]["isprofile"] for key in sv_info]
            )

            nc_out.createVariable("substate_ischebyshev", np.int16, ("substate"))
            nc_out["substate_ischebyshev"][:] = np.array(
                [sv_info[key]["ischebyshev"] for key in sv_info]
            )

        var_id = 1
        nvar = len(list(var_dict.keys()))
        dim_map = {
            "jmx": "atrack",
            "imx": "xtrack",
            "lmx": "level",
            "lmx_e": "layer_edge",
            "xmx": "state",
            "four": "corner",
        }
        chunk_map = {
            "atrack": 101,
            "xtrack": xtrack_chunksize,
            "level": nc_dset_list[0].dimensions["lmx"].size,
            "layer_edge": nc_dset_list[0].dimensions["lmx_e"].size,
            "state": nc_dset_list[0].dimensions["xmx"].size,
            "corner": 4,
        }
        # Main loop over variables
        for key, val in var_dict.items():
            print(
                f"\nNow doing variable {var_id:3d} / {nvar} : {key} {nc_dset_list[0][key].dimensions}"
            )
            var_id += 1
            dtype = np.float32
            if val == "time":
                dtype = np.float64

            try:
                var_dim = tuple(
                    dim_map[dim]
                    for dim in nc_dset_list[0][key].dimensions
                    if dim != "one"
                )
            except:
                print(f"{key} has unsupported dimension. dim_map = {dim_map}")
                continue

            # use the "chunksizes" argument of createVariable to try and speed up the writing of variables
            chunksizes = tuple(chunk_map[dim] for dim in var_dim)

            if val not in nc_out.variables:
                nc_out.createVariable(
                    val, dtype, var_dim, chunksizes=chunksizes, zlib=True
                )
            if val.lower() == "time":
                nc_out[val].units = "hours since 1985-01-01"
                nc_out[val].calendar = "gregorian"

            # loop over datasets
            for dset_id, nc_dset in enumerate(nc_dset_list):
                sys.stdout.write(f"\rGranule {dset_id+1:>4d} / {len(nc_dset_list):>4d}")
                sys.stdout.flush()
                atrack_start = atrack_start_id_list[dset_id]
                atrack_end = atrack_start + nc_dset.dimensions["jmx"].size

                granule_data = da.from_array(nc_dset[key])

                in_atrack_dim_id = nc_dset[key].dimensions.index("jmx")
                in_xtrack_dim_id = nc_dset[key].dimensions.index("imx")
                out_atrack_dim_id = nc_out[val].dimensions.index("atrack")
                out_xtrack_dim_id = nc_out[val].dimensions.index("xtrack")

                if align_file and file_name.startswith("MethaneAIR_L1B_O2"):
                    xtrack_offset = xtrack_offset_list[dset_id]
                    atrack_offset = atrack_offset_list[dset_id]

                    granule_data = da.roll(
                        granule_data, atrack_offset, axis=in_atrack_dim_id
                    )
                    granule_data = da.roll(
                        granule_data, xtrack_offset, axis=in_xtrack_dim_id
                    )

                    if atrack_offset >= 0:
                        atrack_offset_slice = tuple(
                            slice(None)
                            if dim != "atrack"
                            else slice(None, atrack_offset)
                            for dim in nc_out[val].dimensions
                        )
                    else:
                        atrack_offset_slice = tuple(
                            slice(None)
                            if dim != "atrack"
                            else slice(atrack_offset, None)
                            for dim in nc_out[val].dimensions
                        )
                    granule_data[atrack_offset_slice] = netCDF4.default_fillvals["f4"]
                in_slice = [slice(None) for dim in nc_dset[key].dimensions]
                out_slice = [slice(None) for dim in nc_out[val].dimensions]
                out_slice[out_atrack_dim_id] = slice(atrack_start, atrack_end)
                for xtrack_slice in xtrack_chunked_slices:
                    in_slice[in_xtrack_dim_id] = xtrack_slice
                    out_slice[out_xtrack_dim_id] = xtrack_slice
                    nc_out[val][tuple(out_slice)] = (
                        granule_data[tuple(in_slice)].squeeze().compute()
                    )
            # end of loop of datasets
        # end of loop over variables

    print(f"\n{outfile}")
    check_valid_xtrack(outfile)


def upper_str(x: str) -> str:
    return x.upper()


def main():
    parser = argparse.ArgumentParser(
        description="Concatenate a set of L2 files into a single netcdf file without groups and only a subset of variables"
    )
    parser.add_argument("indir", help="full path to the directory containing L2 files")
    parser.add_argument("outfile", help="full path to the output netcdf file")
    parser.add_argument(
        "-s",
        "--search",
        default="MethaneAIR*.nc",
        help="search pattern for file selection (accepts *)",
    )
    parser.add_argument(
        "--align",
        default="",
        type=upper_str,
        choices=[f"RF{i:0>2d}" for i in range(1, 10)],
        help="research flight, will use the corresponding alignment file from /n/holylfs04/LABS/wofsy_lab/Lab/asouri/GEOAkaze/test_mair",
    )
    parser.add_argument(
        "--add",
        nargs="*",
        default=None,
        help="if given, add the given variables to the output file in edit mode",
    )
    args = parser.parse_args()

    if args.align:
        align_path = f"/n/holylfs04/LABS/wofsy_lab/Lab/asouri/GEOAkaze/test_mair/alignment_{args.align.upper()}.txt"
        if not os.path.exists(align_path):
            raise Exception(f"{align_path} does not exist")
    else:
        align_path = ""

    concat_nc(args.indir, args.search, args.outfile, align_path, args.add)


if __name__ == "__main__":
    main()
