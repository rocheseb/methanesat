from typing import Optional, Sequence, Tuple, Union, Annotated, List, Any
from netCDF4 import Dataset
import numpy as np
from datetime import datetime
import glob
import os
import sys
import argparse
import dask
import dask.array as da
from dask.diagnostics import ProgressBar


stored_boxes = {
    "RF04": {"xlim": [-102.4, -102.2], "ylim": [32.0, 32.2],},
    "RF04_road": {"xlim": [-102.3, -102.27], "ylim": [32.04, 32.09]},
    "RF06": {"xlim":[-104.4,-102.9], "ylim":[31.2,32.6]},
    "RF04_prio": {"xlim":[-102.8,-101.4],"ylim":[31.5,32.7]},
    "RF05_prio": {"xlim":[-102.8,-101.4],"ylim":[31.5,32.7]},
    "RF06_prio": {"xlim":[-104.4,-101.6],"ylim":[31.3,32.7]},
    "RF07_prio": {"xlim":[-104.4,-101.6],"ylim":[31.3,32.7]},
    "RF08_prio": {"xlim":[-112.3,-109],"ylim":[39.4,41.2]},
    "RF09_prio": {"xlim":[-103.6,-102.1],"ylim":[47.3,48.6]},
    "RF06_cloud": {"xlim": [-103.17238, -103.06797], "ylim": [37.16525, 37.31407]},
    "RF08": {"xlim": [-112.165, -111.239], "ylim": [40.141, 41.165]},
    "RF02_landfill":{"xlim":[-104.866005, -104.79053],"ylim":[40.56066, 40.63518]},
}


@dask.delayed
def logical_or(x: Any, y: Any) -> bool:
    return bool(x or y)


@dask.delayed(nout=4)
def get_lat_lon(file_path: str, l2: bool = False) -> Tuple[float, float, float, float]:
    if l2:
        grp = "Level1"
    else:
        grp = "Geolocation"
    with Dataset(file_path, "r") as d:
        lon = d[f"{grp}/Longitude"][:]
        lat = d[f"{grp}/Latitude"][:]
    lon0 = np.nanmin(lon)
    lat0 = np.nanmin(lat)
    lonf = np.nanmax(lon)
    latf = np.nanmax(lat)
    return lon0, lat0, lonf, latf


def box_select(
    indir: str,
    outdir: Optional[str] = None,
    xlim: Optional[Annotated[Sequence[float], 2]] = None,
    ylim: Optional[Annotated[Sequence[float], 2]] = None,
    unlink_existing: bool = True,
    box_name: Optional[str] = None,
    save: bool = False,
    suffix: str = ".nc",
    prefix: str = "MethaneAIR",
    l2: bool = False,
    show: bool = False,
) -> List[str]:
    """
    indir: full path to the directory containing L1 files
    outdir: full path to the directory containing the selected subset of L1 files
    xlim: [min,max] longitude in degrees east
    ylim: [min,max] latitude in degrees north
    unlink_existing: if True, unlink preexisting symlinks in outdir
    box_name: one of the keys of stored_boxes to use instead of xlim and ylim
    """
    if show:
        unlink_existing = False
    if (xlim is None or ylim is None) and box_name is None:
        raise Exception("One of xlim+ylim or box_name must be given")
    if box_name is not None:
        if box_name not in stored_boxes:
            raise Exception(f"{box_name} is not in the dictionary of stored boxes")
        xlim = stored_boxes[box_name]["xlim"]
        ylim = stored_boxes[box_name]["ylim"]

    if not os.path.exists(indir):
        raise Exception(f"Wrong path: {indir}")
    if (outdir is not None) and not os.path.exists(outdir):
        raise Exception(f"Wrong path: {outdir}")

    # check the output directory
    if (outdir is not None) and unlink_existing:
        flist = glob.glob(os.path.join(outdir, f"{prefix}*{suffix}"))
        for f in flist:
            os.system("unlink " + f)

    # input directory
    flist = np.array(glob.glob(os.path.join(indir, f"{prefix}*{suffix}")))
    dates = np.array(
        [
            datetime.strptime(os.path.basename(f).split("_")[3], "%Y%m%dT%H%M%S")
            for f in flist
        ]
    )
    sort_ids = np.argsort(dates)
    flist = flist[sort_ids]
    nfiles = len(flist)
    # Loop through files
    out_of_range = []
    xlim = da.from_array(xlim)
    ylim = da.from_array(ylim)
    file_opens = []
    for i, f in enumerate(flist):
        # check if file can be opened:
        try:
            with Dataset(f) as check_file_open:
                pass
        except OSError:
            print(f"Cannot open {f}")
            file_opens.append(False)
        else:
            file_opens.append(True)
    flist = [f for i, f in enumerate(flist) if file_opens[i]]

    for i, f in enumerate(flist):
        lon0, lat0, lonf, latf = get_lat_lon(f, l2)

        # Check for overlap
        lon_check = logical_or(lonf < xlim[0], lon0 > xlim[1])
        lat_check = logical_or(latf < ylim[0], lat0 > ylim[1])
        out_of_range.append(logical_or(lon_check, lat_check))

    with ProgressBar():
        out_of_range = dask.compute(*out_of_range)

    if outdir is not None:
        # make the links
        for i, f in enumerate(flist):
            if not out_of_range[i]:
                print(f"{f}")
                if show:
                    continue
                print(f"ln -s {f} {outdir}")
                os.system(f"ln -s {f} {outdir}")

        if not show:
            link_count = np.count_nonzero(~np.array(out_of_range))
            print(f"{link_count} total links created")

    box_flist = [f for i, f in enumerate(flist) if not out_of_range[i]]

    if save and os.path.exists(os.path.dirname(save)):
        with open(save, "w") as f:
            f.write("\n".join(box_flist))

    return box_flist


def main():
    parser = argparse.ArgumentParser(
        description="Find the subset of L1 files in a given directory that have data within the given lat-lon box"
    )
    parser.add_argument("indir", help="full path to the directory containing L1 files")
    parser.add_argument(
        "-o",
        "--outdir",
        default=None,
        help="full path to the directory containing the selected subset of L1 files",
    )
    parser.add_argument(
        "--xlim",
        nargs=2,
        type=float,
        default=None,
        help="min and max longitude in degrees east",
    )
    parser.add_argument(
        "--ylim",
        nargs=2,
        type=float,
        default=None,
        help="min and max latitude in degrees north",
    )
    parser.add_argument(
        "-u",
        "--no-unlinks",
        action="store_true",
        help="if given, does not unlink preexisting symlinks in outdir",
    )
    parser.add_argument(
        "-b",
        "--box-name",
        help="box name, if given use the xlim and ylim saved in a dictionary",
    )
    parser.add_argument(
        "-s",
        "--save",
        default="",
        help="full path to the file where the list of files will be saved",
    )
    parser.add_argument(
        "--suffix", default=".nc", help="suffix for file pattern search"
    )
    parser.add_argument(
        "--prefix", default="MethaneAIR", help="preffix for file pattern search"
    )
    parser.add_argument(
        "--l2", action="store_true", help="give this argument when using on L2 files"
    )
    parser.add_argument(
        "--show",
        action="store_true",
        help="if given, only print the file names that match the box",
    )
    args = parser.parse_args()

    if not os.path.exists(args.indir):
        raise Exception(f"Wrong path: {args.indir}")
    if (args.outdir is not None) and not os.path.exists(args.outdir):
        raise Exception(f"Wrong path: {args.outdir}")

    flist = box_select(
        args.indir,
        args.outdir,
        args.xlim,
        args.ylim,
        not args.no_unlinks,
        args.box_name,
        args.save,
        args.suffix,
        args.prefix,
        args.l2,
        args.show,
    )

    return flist


if __name__ == "__main__":
    flist = main()
