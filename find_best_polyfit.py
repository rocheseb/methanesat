from pylab import *
from typing import Optional, Sequence, Tuple, Union, Annotated, List, Generator
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import make_pipeline
import numpy as np
from sklearn.model_selection import cross_val_score, train_test_split, GridSearchCV
import random
import numpy.matlib as mlib
import matplotlib.pyplot as plt


def get_knee_idx(x: Union[List[float], np.ndarray]) -> int:
    """
    Get the index of the knee in x.
    """
    npoints = len(x)
    point_coords = np.vstack((range(npoints), x)).T
    np.array([range(npoints), x])
    first_point = point_coords[0]
    line_vec = point_coords[-1] - point_coords[0]
    line_vec_norm = line_vec / np.sqrt(np.sum(line_vec ** 2))
    vec_from_first = point_coords - first_point
    scalar_product = np.sum(
        vec_from_first * mlib.repmat(line_vec_norm, npoints, 1), axis=1
    )
    vec_from_first_parallel = np.outer(scalar_product, line_vec_norm)
    vec_to_line = vec_from_first - vec_from_first_parallel
    dist_to_line = np.sqrt(np.sum(vec_to_line ** 2, axis=1))
    knee_idx = np.argmax(dist_to_line)

    return knee_idx


def PolynomialRegression(degree: int = 2, **kwargs):
    return make_pipeline(
        PolynomialFeatures(degree), StandardScaler(), LinearRegression(**kwargs)
    )


def LinReg(**kwargs):
    return make_pipeline(StandardScaler(), LinearRegression(**kwargs))


def find_best_polyfit(x: np.ndarray, y: np.ndarray, test_size: float = 0.33):

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size)

    degree_list = range(20)
    fit_intercept_list = [True, False]

    best_score = -1e30

    rmse = {}
    for degree in degree_list:
        rmse[degree] = {}
        for fit_intercept in fit_intercept_list:
            poly_features = PolynomialFeatures(degree=degree)
            x_train_poly = poly_features.fit_transform(x_train)
            polynomial_regressor = LinearRegression(fit_intercept=fit_intercept)
            polynomial_regressor.fit(x_train_poly, y_train)
            scores = cross_val_score(
                polynomial_regressor,
                x_train_poly,
                y_train,
                cv=10,
                scoring="neg_root_mean_squared_error",
            )
            print(scores)
            rmse[degree][fit_intercept] = min(abs(scores))
            if max(scores) > best_score:
                best_score = max(scores)
                best_degree = degree
                best_fit_intercept = fit_intercept

    print(f"Best degree: {best_degree}")
    print(f"Best fit_intercept: {best_fit_intercept}")
    print(f"Best score: {best_score}")

    # rmse vs degree
    fig, ax = subplots()
    for fit_intercept in fit_intercept_list:
        ax.plot(
            degree_list,
            [rmse[degree][fit_intercept] for degree in degree_list],
            label=f"fit_intercept={fit_intercept}",
        )
    legend()
    knee_idx = get_knee_idx([rmse[degree][True] for degree in degree_list])
    ax.axvline(x=degree_list[knee_idx], c="red", linestyle="--")
    ax.set_xlabel("Polynomial degree")
    ax.set_ylabel("Best RMSE")
    grid()

    # best fit
    poly_features = PolynomialFeatures(degree=best_degree)
    x_train_poly = poly_features.fit_transform(x_train)
    polynomial_regressor = LinearRegression(fit_intercept=best_fit_intercept)
    x_test_poly = poly_features.fit_transform(x_test)

    y_predict = polynomial_regressor.fit(x_train_poly, y_train).predict(x_test_poly)

    fig, ax = subplots()
    ax.plot(
        x_train, y_train, marker="o", markersize=1, linewidth=0, c="C0", label="train"
    )
    ax.plot(x_test, y_test, marker="o", markersize=1, linewidth=0, c="C1", label="test")
    ids = np.argsort(x_test.ravel())
    ax.plot(x_test.ravel()[ids], y_predict.ravel()[ids], c="red", label="best fit")
    ax.grid()
    ax.set_xlabel("Surface Pressure DoFS")
    ax.set_ylabel("Albedo (1250 nm)")
    legend()

    return polynomial_regressor


def find_best_polyfit_gridsearch(
    x: np.ndarray,
    y: np.ndarray,
    test_size: float = 0.33,
    scoring: str = "neg_root_mean_squared_error",
    refit: bool = True,
    ax: Optional[plt.Axes] = None,
    title: str = "",
    ylab: str = "",
):

    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=test_size)

    param_grid = {
        # "polynomialfeatures__degree": np.arange(20),
        "linearregression__fit_intercept": [True, False],
    }

    # grid = GridSearchCV(PolynomialRegression(), param_grid, cv=10, scoring=scoring,refit=refit)
    grid = GridSearchCV(LinReg(), param_grid, cv=10, scoring=scoring, refit=refit)
    grid.fit(x_train, y_train)

    model = grid.best_estimator_

    y_predict = model.fit(x_train, y_train).predict(x_test)

    if refit is True:
        refit = scoring

    if ax is None:
        fig, ax = subplots()
    ax.set_title(f"{title}, N = {len(x)}")
    ax.plot(
        x_train,
        y_train,
        marker="o",
        markersize=1,
        linewidth=0,
        c="C0",
        label=f"train ({100*(1-test_size)}%)",
    )
    ax.plot(
        x_test,
        y_test,
        marker="o",
        markersize=1,
        linewidth=0,
        c="C1",
        label=f"test ({100*test_size}%)",
    )
    ids = np.argsort(x_test.ravel())
    ax.plot(
        x_test.ravel()[ids],
        y_predict.ravel()[ids],
        c="red",
        label=f"best fit y={model['linearregression'].coef_[0][0]:.3f}x+{model['linearregression'].intercept_[0]:.3f}",
    )  # order = {model['polynomialfeatures'].degree}")
    ax.grid()
    ax.set_ylabel(ylab)
    ax.set_xlabel("ln(Albedo) (1250 nm)")
    # ax.plot(x_test.ravel()[ids],x_test.ravel()[ids],linestyle='--',c='black',label='y=x')
    legend()

    return grid
