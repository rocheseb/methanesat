import sys
import os
import numpy as np
import pandas as pd
from netCDF4 import Dataset
import argparse
from scipy.interpolate import interp1d
import warnings
if 'pysplat' in os.environ:
	sys.path.insert(0,os.environ['pysplat'])
	from generate_lut import write_lut
else:
	warnings.warn("Won't be able to use merge_txt_to_nc")


def main():
	parser = argparse.ArgumentParser(description="Create a list of commands to run the hitran code for each combination of P,T,B using GNU parallel")
	# common arguments
	parser.add_argument('rundir',help='full path to the directory where the hitran line mixing is run')
	parser.add_argument('outdir',help='full path to the directory where the output text files of cross sections are written')
	parser.add_argument('-d','--dims-from',default='',help='full path to an existing netcdf lookup table from which to retrieve the dimensions (P,T,B) OR input json file that contains the pressure/temperature/broadener arrays')
	parser.add_argument('--use-full-lm',action='store_true',help='Switch to full diagonalization line-mixing')
	parser.add_argument('--use-sdv',action='store_true',help='Switch to Speed-dependent Voigt profile')
	parser.add_argument('--wn-lim',nargs=2,type=float,help='wavenumber limits (cm-1)')

	# arguments for generating the list of commands
	parser.add_argument('--dwn',type=float,help='wavenumber spacing (cm-1)')
	parser.add_argument('--xco2',type=float,default=0,help='CO2 mole fraction')
	parser.add_argument('--cutoff',default=1e-27,type=float,help='total band intensity cutoff (cm-1/(molecule.cm-2) at 296K)')
	parser.add_argument('-n','--njobs',default=30,type=int,help='Number of jobs that will be run in parallel')
	parser.add_argument('-r','--run-jobs',action='store_true',help='if given, runs the jobs')

	# arguments for merging the output files
	parser.add_argument('-m','--merge',action='store_true',help='if given, merge the files in outdir into a netcdf lookup table')
	parser.add_argument('--wl-lim',nargs=2,type=float,help='wavelength limits (nm)')
	parser.add_argument('--dwl',default=0.001,type=float,help='wavelength spacing (nm)')
	args = parser.parse_args()

	if args.use_full_lm and args.use_sdv:
		print('Cannot use full-LM diagonalization with SDV, there will only be AbsV and AbsY tables')

	if args.dims_from:
		if args.dims_from.endswith('.nc'):
			with Dataset(args.dims_from) as nc_dset:
				pressure = nc_dset['Pressure'][:]
				temperature = nc_dset['Temperature'][:]
				broadener = nc_dset['Broadener_01_VMR'][:]
		elif args.dims_from.endswith('.json'):
			with open(args.dims_from,'rb') as infile:
				data = json.load(infile)
			pressure = data['pressure']
			temperature = data['temperature']
			broadener = data['broadener']

	if args.merge:
		merge_txt_to_nc(args.rundir,args.outdir,temperature,pressure,broadener,args.wl_lim[0],args.wl_lim[1],args.dwl,args.use_full_lm,args.use_sdv)
		return
	else:
		hitran_lm(args.rundir,args.outdir,args.wn_lim[0],args.wn_lim[1],args.dwn,args.cutoff,args.xco2,temperature,pressure,broadener,int(args.use_full_lm),int(args.use_sdv),args.njobs,args.run_jobs)


def hitran_lm(rundir,outdir,wns,wnf,dwn,cutoff,xco2,temperature,pressure,broadener,use_full_lm,use_sdv,njobs,run_jobs):
	"""
	Create a list of commands to run the hitran code for each combination of P,T,B
	"""
	commands_list = []
	datadir = os.path.join(rundir,'data_new')

	profile = ''
	if use_sdv:
		profile += '_SDVP'
	else:
		profile += '_VP'
	if use_full_lm:
		profile += '_LM'

	npres = pressure.size
	nT = temperature[0].size
	nB = broadener.size
	for iP,P in enumerate(pressure):
		for iT,T in enumerate(temperature[iP]):
			for iB,xh2o in enumerate(broadener):
				outfile = os.path.join(outdir,f"CO2_{iP}_{iT}_{iB}{profile}.out")
				commands_list += [f"cd {rundir}; ./main.exe {wns} {wnf} {dwn} {cutoff} {T} {P/101325} {xco2} {xh2o:.2f} {use_full_lm} {use_sdv} {outfile} {datadir} >/dev/null\n"]

	commands_file = os.path.join(rundir,'hitran_lm_parallel.txt')
	with open(commands_file,'w') as outfile:
		outfile.writelines(commands_list)
	print(f'Created {commands_file}')

	ntot_jobs = npres*nT*nB
	if run_jobs:
		print(f'Running {ntot_jobs} jobs with GNU parallel with max {njobs} jobs at a time')
		proc = subprocess.Popen(f'parallel -j {njobs} < {commands_file}',stdout=subprocess.PIPE,shell=True)


def progress(i,tot,bar_length=20,word=''):
    """
    a fancy loadbar to be displayed in the prompt while executing a time consuming loop
    """
    if tot==0:
        tot=1
    percent=float(i+1)/tot
    hashes='#' * int(round(percent*bar_length))
    spaces=' ' * (bar_length - len(hashes))

    if i+1==tot:
        sys.stdout.write("\rProgress:[{0}] {1}%".format(hashes + spaces, int(round(percent * 100)))+" "*50+'\n')
    else:
        sys.stdout.write("\rProgress:[{0}] {1}%".format(hashes + spaces, int(round(percent * 100)))+"    "+str(i+1)+"/"+str(tot)+" "+word+"   ")
    sys.stdout.flush()


def merge_txt_to_nc(rundir,outdir,temperature,pressure,broadener,wls,wlf,dwl,use_full_lm,use_sdv):
	"""
	Reads a collections of files written by the hitran LM codes and save all the data to a netcdf table
	"""
	npres = pressure.size
	nT = temperature[0].size
	nB = broadener.size

	wl = np.arange(wls,wlf,dwl)
	nwl = wl.size
	xs = {abs:np.zeros((npres,nT,nB,nwl)) for abs in ['absv','absy','absw']}
	if use_sdv:
		del xs['absw'] # can't use full line mixing with sdvp

	profile = ''
	if use_sdv:
		profile += '_SDVP'
	else:
		profile += '_VP'
	if use_full_lm:
		profile += '_LM'

	ntot = npres*nT*nB*len(xs.keys())
	count = 1
	for iP,P in enumerate(pressure):
		for iT,T in enumerate(temperature[iP]):
			for iB,xh2o in enumerate(broadener):
				data = pd.read_csv(os.path.join(rundir,f"CO2_{iP}_{iT}_{iB}{profile}.out"),delim_whitespace=True,names=['wnum','absv','absy','absw'])
				data['wl'] = 1e7/data['wnum'] # converts cm-1 to nm
				data = data.sort_values(by='wl')
				for abs in xs:
					progress(count,ntot)
					count += 1
					f = interp1d(data['wl'],data[abs])
					xs[abs][iP,iT,iB] = f(wl) # interpolate onto the desired wavelength grid

	for abs in xs:
		profile = ''
		if use_sdv:
			profile += '_SDVP'
		else:
			profile += '_VP'
		if abs == 'absy':
			profile += '_LM'
		elif abs == 'absw':
			profile += '_fullLM'

		outfile = os.path.join(outdir,f"HITRAN2020_CO2_{wls:.0f}_{wlf:.0f}{profile}.nc")
		write_lut(outfile, pressure, temperature, broadener, wl, xs[abs])


if __name__=="__main__":
	main()
