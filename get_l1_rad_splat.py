from typing import Optional, Sequence, Tuple, Union, Annotated, List, Generator
from netCDF4 import Dataset
from scipy.io import loadmat
from scipy.special import gamma
import os
import glob
import numpy as np
import argparse


def supergauss(x: np.ndarray, par: Union[List, np.ndarray]) -> np.ndarray:
    """
    Supergaussian line shape
    """
    # Prefactor
    A = par[1] / (2.0 * par[0] * gamma(1.0 / par[1]))

    return A * np.exp(-1.0 * np.power(np.abs(x / par[0]), par[1]))


def get_l1_rad_splat(
    indir: str,
    outdir: str,
    tag: str,
    search: str,
    mode: str,
    ref_wvl: float,
    irr_infile: Optional[str] = None,
) -> None:
    """
    Read L1 files and creates a netcdf file with the corresponding surface albedo or average radiance

    indir: full path to the directory with the L1 files
    outdir: full path to the directory where the output file will be saved
    tag: the output filename will be mode_ref-wvlnm_tag.nc
    search: search pattern for the L1 files
    irr_infile: full path to the irradiance file
    mode: one of [radiance,albedo]
    ref_wvl: reference wavelength for the albedo calculation
    """

    for path in [indir, outdir, irr_infile]:
        if not os.path.exists(path):
            raise Exception(f"Wrong path: {path}")

    outfile = os.path.join(outdir, f"{mode}_{int(ref_wvl)}nm_{tag}.nc")

    hw1e = 0.14
    wvl_hw = 2

    do_alb = mode == "albedo"
    if do_alb and (irr_infile is None):
        raise Exception("irr_infile must be given in the albedo mode")

    print(indir)

    if do_alb:
        out_fld = "alb"

        # Load Solar Spectrum
        with Dataset(irr_infile, "r") as d:
            wvl_irr = d.variables["Wavelength"][:]
            spc_irr = d.variables["Irradiance"][:]

        # Get irradiance
        idv = np.where(
            np.logical_and(
                wvl_irr >= ref_wvl - 10.0 * hw1e, wvl_irr <= ref_wvl + 10.0 * hw1e
            )
        )[0]
        idv_p1 = [i + 1 for i in idv]
        x = wvl_irr[idv] - ref_wvl
        y = spc_irr[idv]
        dx = wvl_irr[idv_p1] - wvl_irr[idv]
        par = np.zeros(2)
        par[0] = hw1e
        par[1] = 2.0
        F0 = np.sum(y * supergauss(x, par) * dx)
        prefac = np.pi / F0
    else:
        out_fld = "rad"

    # Get list of files
    flist = glob.glob(os.path.join(indir, search))
    flist.sort()

    # Check files
    flist_old = flist
    flist = []
    for f in flist_old:
        print("Checking: " + f)
        try:
            with Dataset(f) as d:
                if "Wavelength" in d["Band1"].variables.keys():
                    flist.append(f)
        except:
            print("problem with file")

    tau = []
    rad = []
    lon = []
    lat = []
    for f in flist:

        print(f)

        # Read data
        with Dataset(f) as d:
            lon.append(d["Geolocation"]["Longitude"][:].T)
            lat.append(d["Geolocation"]["Latitude"][:].T)
            tau.append(d["Geolocation"]["Time"][:])

            wvl = np.nanmedian(d["Band1"]["Wavelength"][:], axis=1)
            xmx = wvl.shape[1]
            tmx = tau[-1].shape[0]

            if do_alb:
                mu = np.cos(np.deg2rad(d["Geolocation"]["SolarZenithAngle"][:].T))

            this_rad = np.ones((xmx, tmx)) * np.nan
            for x in range(xmx):

                # Get radiance
                iwc = np.argmin(np.abs(wvl[:, x] - ref_wvl))
                print(x, iwc, d["Band1"]["Radiance"].shape)
                this_rad[x, :] = np.nanmean(
                    d["Band1"]["Radiance"][
                        iwc - wvl_hw : iwc + wvl_hw + 1, :, x
                    ].squeeze(),
                    axis=0,
                )

        if do_alb:
            rad.append(prefac / mu * this_rad)
        else:
            rad.append(this_rad)

    tau = np.concatenate(tau, axis=0)
    rad = np.concatenate(rad, axis=1)
    lon = np.concatenate(lon, axis=1)
    lat = np.concatenate(lat, axis=1)

    # Write file
    with Dataset(outfile, "w") as d:
        d.createDimension("x", lon.shape[0])
        d.createDimension("t", lon.shape[1])
        v = d.createVariable("lon", np.float32, ("x", "t"))
        v[:] = lon
        v = d.createVariable("lat", np.float32, ("x", "t"))
        v[:] = lat
        v = d.createVariable(out_fld, np.float32, ("x", "t"))
        v[:] = rad
        v = d.createVariable("tau", np.float64, ("t"))
        v[:] = tau


def main():
    parser = argparse.ArgumentParser(
        description="Read L1 files and creates a netcdf file with the corresponding surface albedo or average radiance"
    )
    parser.add_argument("indir", help="full path to the directory with the L1 files")
    parser.add_argument(
        "outdir", help="full path to the directory where the output file will be saved"
    )
    parser.add_argument("tag", help="the output filename will be mode_ref-wvlnm_tag.nc")
    parser.add_argument(
        "-s",
        "--search",
        default="MethaneAIR_L1B_CH4*.nc",
        help="search pattern for the L1 files",
    )
    parser.add_argument(
        "--irr-infile",
        default="/n/holyscratch01/wofsy_lab/sroche/o2_test/static_datasets/SPLAT/data/SolarSpectra/chance_jpl_mrg_hitran_200-16665nm.nc",
        help="full path to the irradiance file",
    )
    parser.add_argument(
        "--mode",
        default="albedo",
        choices=["radiance", "albedo"],
        help="one of [radiance,albedo]",
    )
    parser.add_argument(
        "--ref-wvl",
        default="1250",
        type=float,
        help="reference wavelength for the albedo calculation",
    )
    args = parser.parse_args()

    get_l1_rad_splat(
        args.indir,
        args.outdir,
        args.tag,
        args.search,
        args.mode,
        args.ref_wvl,
        args.irr_infile,
    )


if __name__ == "__main__":
    main()
